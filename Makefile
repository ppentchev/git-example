all:		README.html

README.html:	README.txt
		markdown README.txt > README.html || (rm -f README.html; false)

clean:
		rm -f README.html
